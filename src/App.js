
import './App.css';
import CakeShop from './components/CakeShop';
import { Provider} from "react-redux"
import store from './redux/store'
import CakeHook from './hook/CakeHook';
import IcacreamShop from './components/IcacreamShop';
import NewBuyCake from './components/NewBuyCake';
import ItemContent from './components/ItemContent';
import UserComponent from './components/userComponent';

function App() {
  return (
    <Provider store={store}> 
    <div className="App">
    {/* <ItemContent cake/>
    <ItemContent/>
     <CakeShop/>
     <CakeHook/>
     <IcacreamShop/>
     <NewBuyCake/> */}
     <UserComponent/>
    </div>
    </Provider>
  );
}

export default App;
