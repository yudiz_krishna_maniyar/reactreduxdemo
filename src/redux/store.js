import {createStore, applyMiddleware} from "redux";
import {composeWithDevTools} from 'redux-devtools-extension'
import rootReducer from './rootReducer';
import thunk from "redux-thunk";

// const logger = createLogger ()
const middleware = applyMiddleware ( thunk);
const store = createStore (rootReducer,composeWithDevTools( middleware));


// const store = createStore(rootReducer, applyMiddleware(createLogger))

export default store