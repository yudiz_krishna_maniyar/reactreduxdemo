import { USER_REQUEST,USER_SUCCESS,USER_FAILUAR } from './userType'
import axios from "axios"

  export const userrequest = () => {
  return {
    type: USER_REQUEST,
  };
};
  export const usersuccess = user => {
  return {
    type: USER_SUCCESS,
    payload: user,
  };
};

   export const userfail = error => {
  return {
    type: USER_FAILUAR,
    payload: error,
  };
};

export const featchdata = () => {
  return  (dispatch)=> {
    dispatch (userrequest ());
    axios
      .get ('https://jsonplaceholder.typicode.com/users')
      .then (response => {
        const user = response.data
        dispatch (usersuccess (user))
      })
      .catch (error => {
        const errormsg = error.message
        dispatch (userfail (errormsg))
      });
  };
};
