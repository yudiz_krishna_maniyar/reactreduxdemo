import {USER_REQUEST, USER_SUCCESS, USER_FAILUAR} from './userType'



const InitalState = {
  loading: true,
  user: [],
  error: '',
};


const reducer = (state = InitalState, action) => {
  switch (action.type) {
    case USER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case USER_SUCCESS:
      return {
        loading: false,
        user: action.payload,
        error: '',
      };
    case USER_FAILUAR:
      return {
        loading: true,
        user: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer