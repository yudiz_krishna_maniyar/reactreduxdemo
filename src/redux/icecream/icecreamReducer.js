import {ICE_CREAM} from './icecreamType'

const InitialState = {
  numOfIcecream : 20
}

const icecreamReducer = (state = InitialState, action) => {
    switch (action.type) {
        case ICE_CREAM:
            return {
                ...state,
                numOfIcecream: state.numOfIcecream -1
            }
       
        default:
            return state
    }
}

export default icecreamReducer