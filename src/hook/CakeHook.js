import React from 'react'
import {useSelector, useDispatch} from "react-redux"
import {buycake} from '../redux/cake'

const CakeHook = () => {
    const numofcake = useSelector(state => state.cake.numOfCake)
    const dispatch = useDispatch()
  return (
    <>
    <div>useCakeHook- {numofcake}
    <button onClick={()=> dispatch(buycake())}>click</button></div>
    </>
  )
}

export default CakeHook