import React from 'react'
import { buycake} from '../redux/cake';
import {connect} from 'react-redux'

const CakeShop = (props) => {
  return (
    <>
    <div >CakeShop {props.numOfCake}
    <button onClick={props.buycake}>click</button>
    </div>
    </>
  )
}

const mapStateToProp = state=>{
    return{
        numOfCake : state.cake.numOfCake
    }
}
const mapdispatchToProp = dispatch => {
  return {
   buycake: ()=>dispatch(buycake())
  };
};



export default connect(mapStateToProp, mapdispatchToProp)(CakeShop)