import React, { useEffect } from "react";
import { connect } from "react-redux";
import { featchdata } from "../redux/users";

const UserComponent = ({ userdata, featchdata }) => {
  useEffect(() => {
    featchdata();
  }, []);
  return userdata.loading ? (
    <h2>loading</h2>
  ) : userdata.error ? (
    <h2>{userdata.error}</h2>
  ) : (
    <div>
      <h2>user list</h2>
      <div>
        {userdata &&
          userdata.user &&
          userdata.user.map((users) => <p>{users.name}</p>)}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    userdata: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    featchdata: () => {
      dispatch(featchdata());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserComponent);
