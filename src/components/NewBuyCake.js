import React,{useState} from 'react'
import { buycake} from '../redux/cake';
import {connect} from 'react-redux'


const NewBuyCake = (props) => {
    const [number , setNumber]= useState(1)
  return (
    <div>
    <div >NewBuyCake {props.numOfCake}
    
    <input type='text'  onChange={(e)=>setNumber(e.target.value)}/> 
    <button onClick={()=>props.buycake (number)}>buy cake {number}</button>

    </div>
    </div>
  )
}

const mapStateToProp = state=>{
    return{
        numOfCake : state.cake.numOfCake
    }
}
const mapdispatchToProp = dispatch => {
  return {
   buycake: number =>dispatch(buycake(number))
  };
};



export default connect(mapStateToProp, mapdispatchToProp)(NewBuyCake)