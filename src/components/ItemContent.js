import React from 'react'
import {connect} from 'react-redux'
import { buycake } from '../redux/cake'
import { buyicecream } from '../redux/icecream'

const ItemContent = (props) => {
  return (
    <div>ItemContent-{props.item}
<button onClick={props.buyitem}>buy item </button>
    </div>
  )
}

const mapStateToProps =( state, ownprops)=>{
 const items = ownprops.cake ? state.cake.numOfCake : state.icecream.numOfIcecream
return {
    item  : items
}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    const dispatchFunction = ownProps.cake ? ()=> dispatch(buycake()): ()=> dispatch (buyicecream())
     return {
       buyitem : dispatchFunction
        }
    }


export default connect(mapStateToProps, mapDispatchToProps)(ItemContent)