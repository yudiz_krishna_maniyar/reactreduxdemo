import React from 'react'
import { buyicecream} from '../redux/icecream';
import {connect} from 'react-redux'

const IcecreamShop = (props) => {
  return (
    <>
    <div >CakeShop {props.numOfIcecream}
    <button onClick={props.buyicecream}>click</button>
    </div>
    </>
  )
}

const mapStateToProp = state=>{
    return{
        numOfIcecream : state.icecream.numOfIcecream
    }
}
const mapdispatchToProp = dispatch => {
  return {
   buyicecream: ()=>dispatch(buyicecream())
  };
};



export default connect(mapStateToProp, mapdispatchToProp)(IcecreamShop)